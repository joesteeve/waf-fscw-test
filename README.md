A bare-bones project to test the 'fscw_hc08' waf tool.

Ensure that 'python' is in the PATH. Then, to build:

       $ ./waf configure --fscw-mcu-root=/path/to/Freescale/Codewarrior/MCU
       $ ./waf
