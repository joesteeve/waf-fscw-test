# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

APPNAME = 'waftest'
VERSION = '0.1'

import os
from os import path

top = '.'
out = 'build'

def options(opt):
    opt.load('fscw_hc08', tooldir="waftools")
    opt.add_option("--enable-debug", action="store_true", default=False,
                   help="Enable debug build")

def configure(conf):
    conf.load('fscw_hc08', tooldir="waftools")
    conf.env.ENABLE_DEBUG_BUILD = conf.options.enable_debug
    conf.env.APP_NAME = "waftest"

def build(bld):
    bld.recurse('src')

# Local Variables:
# mode: python
# indent-tabs-mode: nil
# tab-width: 4
# End:
