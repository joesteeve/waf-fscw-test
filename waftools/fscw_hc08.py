# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import os
from waflib import Task, TaskGen, Errors
from waflib.Tools import ccroot, ar, c_preproc
from waflib.Configure import conf


def options(opt):
    opt.add_option(
        "--fscw-mcu-root", type='string', default='', dest='fscw_root',
        help="Path to the 'Freescale CodeWarrior MCU' installation")

def configure(conf):
    if not conf.options.fscw_root:
        conf.fatal("Path to the Freescale CodeWarrior MCU installation should be set with the --fscw-mcu-root option")
    conf.find_fscw_hc08()

@conf
def find_fscw_hc08(conf):
    prog_dir = os.path.join(conf.options.fscw_root, 'prog')
    conf.find_program('chc08', path_list=[prog_dir,], var='CC')
    conf.find_program('linker', path_list=[prog_dir,], var='LINK_CC')

    v = conf.env
    v['FSCW_ROOT'] = conf.options.fscw_root
    v['FSCW_CFLAGS'] = ["-BfaTSRON", "-Qvtpnone",
                        "-WmsgFob\"%f%e:%l:%k:%d %m\n\"", "-Cs08"]
    v['FSCW_LINKFLAGS'] = ["-M", "-WmsgFob\"%f%e:%l:%k:%d %m\n\"",
                           "-WmsgSd1100", "-WmsgSd1912"]
    v['CC_TGT_ST'] = '-ObjN="%s"'
    v['CCLNK_TGT_ST'] = '-O"%s"'
    v['CPPPATH_ST']          = '-I"%s"'
    v['DEFINES_ST']          = '-D%s'

    # program
    v['fscw_hc08_image_PATTERN']    = '%s.abs'

    v.append_unique('DEFINES', '__NO_FLOAT__')
    add_includes(conf.options.fscw_root, v)
    add_libs(conf.options.fscw_root, v)

def add_includes(root, env):
    base_parts = ("lib", "hc08c")
    inc_parts = (("device", "src"),
                 ("lib",),
                 ("src",),
                 ("device", "include"),
                 ("include",),
                 ("device", "asm_include"))
    base = os.path.join(root, *base_parts)
    for inc_part in inc_parts:
        include = os.path.join(base, *inc_part)
        env.append_unique('INCLUDES', include)

def add_libs(root, env):
    base_parts = ("lib", "hc08c", "lib")
    base = os.path.join(root, *base_parts)
    env.append_unique('STLIB', os.path.join(base, 'ansiis.lib'))

@TaskGen.extension('.c')
def c_hook(self, node):
    "Bind the c file extension to the creation of a :py:class:`waflib.Tools.c.c` instance"
    return self.create_compiled_task('c', node)

class c(Task.Task):
    "Compile C files into object files"
    run_str = '${CC} ${FSCW_CFLAGS} ${CFLAGS} ${CPPFLAGS} ${CPPPATH_ST:INCPATHS} ${DEFINES_ST:DEFINES} ${SRC} ${CC_TGT_ST:TGT}'
    vars    = ['CCDEPS'] # unused variable to depend on, just in case
    ext_in  = ['.h'] # set the build order easily by using ext_out=['.h']
    scan    = c_preproc.scan

class fscw_hc08_image(ccroot.link_task):
    "Link object files into a c program"
    run_str = '${LINK_CC} ${FSCW_LINKFLAGS} ${LINKER_SCRIPT} -Add(${SRC} ${STLIB}) ${CCLNK_TGT_ST:TGT}'
    ext_out = ['.abs']
    vars    = ['LINKDEPS']
    inst_to = '${BINDIR}'


@TaskGen.feature('fscw_hc08_image')
@TaskGen.before_method('process_source')
def fscw_process_params(self):
    linker_script = getattr(self, 'linker_script', None)
    if not linker_script:
        raise Errors.WafError('fscw_hc08_image requires linker_script')
    self.env['LINKER_SCRIPT'] = linker_script.abspath()

    memory_model = getattr(self, 'memory_model', 'small')
    if memory_model == 'banked':
        self.env.append_unique('FSCW_CFLAGS', '-Mb')
    elif memory_model == 'small':
        self.env.append_unique('FSCW_CFLAGS', '-Ms')
    elif memory_model == 'tiny':
        self.env.append_unique('FSCW_CFLAGS', '-Mt')
    else:
        raise Errors.WafError('Unknown memory_model=%s' % memory_model)

# Local Variables:
# mode: python
# indent-tabs-mode: nil
# tab-width: 4
# End:
